package moja.apps233.com.collectors.ui_utilities;

import android.app.AlertDialog;
import android.content.Context;
import android.widget.ArrayAdapter;

/**
 * Created by Albert on 02/12/2014.
 */
public class SelectBox {

    private AlertDialog selectDialog;
    private String selectedItem;
    private int selectedIndex;

    public SelectBox(Context context, String title, ArrayAdapter<String> adapter) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(true);

        //Set List Adapter
        builder.setAdapter(adapter, (dialog, id) -> {
            setSelectedIndex(id);
            setSelectedItem(adapter.getItem(id));
        });

        selectDialog = builder.create();
    }

    public void show(){
        if (selectDialog != null){
            selectDialog.show();
        }
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(String selectedItem) {
        this.selectedItem = selectedItem;
    }
}
