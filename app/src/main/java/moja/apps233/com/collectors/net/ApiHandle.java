package moja.apps233.com.collectors.net;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import moja.apps233.com.collectors.util.Config;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by selasiehanson on 27/11/2015.
 */
public class ApiHandle {


    public ApiHandle() {

    }

    public static Retrofit getRetrofit(final String token) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okClient = new OkHttpClient();

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        okClient.interceptors().add(chain -> {
            Response response = chain.proceed(chain.request());
            return response;
        });

        okClient.interceptors().add(chain -> {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header(Config.X_AUTH_TOKEN, token)
                    .header(Config.X_AUTH_PROVIDER, Config.AuthProviders.LOCAL)
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        });

        okClient.interceptors().add(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .client(okClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit;
    }
}
