package moja.apps233.com.collectors.ui_utilities;

import android.app.AlertDialog;
import android.content.Context;

import moja.apps233.com.collectors.R;


/**
 * Created by selasiehanson on 11/20/14.
 */
public class OkBox {


    private AlertDialog alert;

    public OkBox(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setIcon(R.mipmap.ic_report_problem_black);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", (dialog, id) -> dialog.dismiss());
        alert = builder.create();
    }

    public void show() {
        if (alert != null) {
            alert.show();
        }
    }
}
