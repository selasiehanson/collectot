package moja.apps233.com.collectors.util;

/**
 * Created by selasiehanson on 2/1/15.
 */
public class Config {
    public static final String X_AUTH_TOKEN = "X-AUTH-TOKEN";
    public static final String X_AUTH_PROVIDER = "X-AUTH-PROVIDER";

    public static final String STORE_KEY = "GLOBAL_STORE";
    public static final String USER_TOKEN = "TOKEN";

    public static final String USER_EMAIL = "USER_EMAIL";
    public static final String PASSWORD = "PASSWORD";
    public static final String LAST_LOGIN_TIME = "LAST_LOGIN_TIME";

    //    public static final String BASE_URL = "http://10.0.3.2:9000/"; //use this if u r using genymotion
    // public static final String BASE_URL = "http://10.0.2.2:9000/"; //use this if u r using emulator
    // public static final String BASE_URL = "http://192.168.1.6:9000/";
    public static final String BASE_URL = "https://mojatest.herokuapp.com/";

    //if using device find your machine ip and use that instead

    public static class AuthProviders {
        public static final String GOOGLE = "GOOGLE";
        public static final String FACEBOOK = "FACEBOOK";
        public static final String MOBILE = "MOBILE";
        public static final String LOCAL = "LOCAL";
    }

    public static final class DATE_FORMATS {
        public static final String MM_DD_YYYY = "MM-dd-yyyy";
        public static final String YEAR_MONTH_DAY = "yyyy-MM-dd";
        public static final String FORM_DATE = "MMM d, yyyy";
    }
}
