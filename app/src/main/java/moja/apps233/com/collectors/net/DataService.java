package moja.apps233.com.collectors.net;

import java.util.List;

import moja.apps233.com.collectors.net.request_objects.DonationDTO;
import moja.apps233.com.collectors.net.request_objects.UserLoginReq;
import moja.apps233.com.collectors.net.response_objects.DonationResponse;
import moja.apps233.com.collectors.net.response_objects.LoginResponse;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by selasiehanson on 09/02/2016.
 */
public interface DataService {

    @POST("/api/login")
    Call<LoginResponse> login(@Body UserLoginReq userLoginReq);

    @POST("/api/donations/bulk-uploads")
    Call<DonationResponse> uploadDonations(@Body List<DonationDTO> donationDTOs);
}
