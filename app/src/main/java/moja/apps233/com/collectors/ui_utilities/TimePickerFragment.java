package moja.apps233.com.collectors.ui_utilities;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by Albert on 14/12/2014.
 */
public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    static Context sContext;
    static Calendar sTime;
    static TimeDialogFragmentListener sListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        int hour = sTime.get(Calendar.HOUR_OF_DAY);
        int minute = sTime.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Calendar newTime = Calendar.getInstance();
        newTime.set(hourOfDay, minute);
        sListener.dateDialogFragmentDateSet(newTime);
    }


    public static TimePickerFragment newInstance(Context context, int titleResource, Calendar time) {
        TimePickerFragment dialog = new TimePickerFragment();
        sContext = context;
        sTime = time;
        Bundle data = new Bundle();
        data.putInt("title", titleResource);
        dialog.setArguments(data);
        return dialog;
    }


    public void setTimeDialogFragmentListener(TimeDialogFragmentListener listener) {
        sListener = listener;
    }
}