package moja.apps233.com.collectors.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Calendar;

import moja.apps233.com.collectors.models.User;

/**
 * Created by selasiehanson on 09/02/2016.
 */
public class StorageUtil {

    public static Long getDonationCentreId(Context context) {
        return 1L;
    }

    public static void storeUserDetails(Context context, User user) {
        SharedPreferences preferences = context.getSharedPreferences(Config.STORE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(Config.USER_TOKEN, user.getToken());

        editor.putString(Config.USER_EMAIL, user.getEmail());
        editor.putString(Config.PASSWORD, user.getPassword());
        editor.putLong(Config.LAST_LOGIN_TIME, getCurrentTimeInMilliseconds());
        editor.commit();
    }

    public static Long getCurrentTimeInMilliseconds() {
        return Calendar.getInstance().getTimeInMillis();
    }

    public static Long getLastLongInTime(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Config.STORE_KEY, context.MODE_PRIVATE);
        return preferences.getLong(Config.LAST_LOGIN_TIME, -1L);
    }

    public static String getToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Config.STORE_KEY, context.MODE_PRIVATE);
        return preferences.getString(Config.USER_TOKEN, "");
    }
}
