package moja.apps233.com.collectors.sync;

/**
 * Created by selasiehanson on 13/02/2016.
 */
public class MessageEvent {
    public final String message;

    public MessageEvent(String message) {
        this.message = message;
    }
}
