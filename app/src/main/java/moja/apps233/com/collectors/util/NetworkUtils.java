package moja.apps233.com.collectors.util;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by selasiehanson on 12/02/2016.
 */
public class NetworkUtils {

    public static boolean isDeviceOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}

