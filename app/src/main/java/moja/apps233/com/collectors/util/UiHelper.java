package moja.apps233.com.collectors.util;

import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by selasiehanson on 12/02/2016.
 */
public class UiHelper {
    public static void showTextError(TextView tv, String message) {
        tv.setError(message);
    }

    public static String errorForField(String field, String message) {
        return String.format("%s %s", field, message);
    }

    public static String formatDate(Date date, String format) {
        if (format.isEmpty()) format = Config.DATE_FORMATS.FORM_DATE;
        SimpleDateFormat dateFormater = new SimpleDateFormat(format);
        return dateFormater.format(date.getTime());
    }

    public static Calendar fromFormDateToJavaDate(String dateStr) {

        DateFormat format = new SimpleDateFormat(Config.DATE_FORMATS.FORM_DATE, Locale.ENGLISH);
        Calendar cal = null;
        try {
            Date date = format.parse(dateStr);
            cal = Calendar.getInstance();
            cal.setTime(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }

}
