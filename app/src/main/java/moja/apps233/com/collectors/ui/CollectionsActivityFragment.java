package moja.apps233.com.collectors.ui;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.activeandroid.content.ContentProvider;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import moja.apps233.com.collectors.R;
import moja.apps233.com.collectors.models.Donation;
import moja.apps233.com.collectors.sync.MessageEvent;

/**
 * A placeholder fragment containing a simple view.
 */
public class CollectionsActivityFragment extends Fragment {

    DonationCursorAdapter donationAdapter;

    public CollectionsActivityFragment() {
    }

    public void createAdapter() {
        Cursor donationCursor = Donation.donationCursor();
        donationAdapter = new DonationCursorAdapter(getActivity(), donationCursor, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_collections, container, false);

        ListView lvItems = (ListView) rootView.findViewById(R.id.donationsListView);

        createAdapter();
        lvItems.setAdapter(donationAdapter);

        getActivity().getSupportLoaderManager().initLoader(0, null, new LoaderManager.LoaderCallbacks<Cursor>() {
            @Override
            public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                return new CursorLoader(getActivity(),
                        ContentProvider.createUri(Donation.class, null),
                        null, null, null, null
                );
            }

            @Override
            public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
                donationAdapter.swapCursor(data);
            }

            @Override
            public void onLoaderReset(Loader<Cursor> loader) {
                donationAdapter.swapCursor(null);
            }


        });
        EventBus.getDefault().register(this);

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
//        if (donationAdapter == null)
//            createAdapter();
    }


//    @Override
//    public void onStop() {
//        EventBus.getDefault().unregister(this);
//        super.onStop();
//    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handle(MessageEvent event) {

        Toast.makeText(getActivity(), event.message, Toast.LENGTH_SHORT).show();
    }

}
