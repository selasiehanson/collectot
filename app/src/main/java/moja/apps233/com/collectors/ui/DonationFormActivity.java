package moja.apps233.com.collectors.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.activeandroid.query.Select;

import java.util.Calendar;

import moja.apps233.com.collectors.R;
import moja.apps233.com.collectors.models.Donation;
import moja.apps233.com.collectors.ui_utilities.DateDialogFragmentListener;
import moja.apps233.com.collectors.ui_utilities.DatePickerFragment;
import moja.apps233.com.collectors.util.Config;
import moja.apps233.com.collectors.util.StorageUtil;
import moja.apps233.com.collectors.util.UiHelper;

public class DonationFormActivity extends AppCompatActivity {

    private EditText firsNameTV;
    private EditText lastNameTV;
    private EditText phoneNumberTV;
    private TextView dateOfBirthTV;
    private Spinner bloodGroupSp;
    private Spinner genderSP;
    private Button dateOfBirthBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> saveDonation(view));
        setupComponents();
        int count = new Select().from(Donation.class).count();
        Log.d("DONATION-COUNT", String.format("%d", count));
    }

    private void setupComponents() {
        genderSP = (Spinner) findViewById(R.id.genderSP);
        bloodGroupSp = (Spinner) findViewById(R.id.bloddGroupSP);
        firsNameTV = (EditText) findViewById(R.id.firstName);
        lastNameTV = (EditText) findViewById(R.id.lastName);
        phoneNumberTV = (EditText) findViewById(R.id.phoneNumber);
        dateOfBirthTV = (TextView) findViewById(R.id.dateOfBirth);
        dateOfBirthBtn = (Button) findViewById(R.id.dateOfBirthBtn);

        dateOfBirthBtn.setOnClickListener((v) -> {
            Calendar _date = Calendar.getInstance();
            if (!dateOfBirthTV.getText().toString().isEmpty()) {
                _date = UiHelper.fromFormDateToJavaDate(dateOfBirthTV.getText().toString());
            }
            DatePickerFragment dateDialog = DatePickerFragment.newInstance(this, 0, _date);
            dateDialog.setDateDialogFragmentListener(new DateDialogFragmentListener() {
                @Override
                public void dateDialogFragmentDateSet(Calendar date) {
                    String d = UiHelper.formatDate(date.getTime(), Config.DATE_FORMATS.FORM_DATE);
                    dateOfBirthTV.setText(d);

                    date.set(Calendar.HOUR, 0);
                    date.set(Calendar.MINUTE, 0);
                    date.set(Calendar.SECOND, 0);
                    date.set(Calendar.MILLISECOND, 0);
                }
            });
            dateDialog.show(this.getSupportFragmentManager(), "DATE_REQUIRED");
        });
    }

    private void saveDonation(View view) {
        if (isFormValid()) {
            Donation donation = new Donation();
            donation.setFirstName(firsNameTV.getText().toString());
            donation.setLastName(lastNameTV.getText().toString());
            donation.setPhoneNumber(phoneNumberTV.getText().toString());
            donation.setDateOfBirth(dateOfBirthTV.getText().toString());
            donation.setDonationCentreId(StorageUtil.getDonationCentreId(this));
            donation.setGender(genderSP.getSelectedItem().toString());
            donation.setBloodGroup(bloodGroupSp.getSelectedItem().toString());
            donation.setDonationDate(System.currentTimeMillis());
            donation.localSave();

            Snackbar.make(view, "Donation saved successfully", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            gotToCollectionListPage();
        }


    }

    public void gotToCollectionListPage() {
        Intent collectionsActivityIntent = new Intent(getApplicationContext(), CollectionsActivity.class);
        startActivity(collectionsActivityIntent);
        finish();
    }

    public boolean isFormValid() {
        String empty = getStringResource(R.string.empty_value);
        if (firsNameTV.getText().toString().isEmpty()) {
            UiHelper.showTextError(firsNameTV, UiHelper.errorForField("First name", empty));
            return false;
        }

        if (lastNameTV.getText().toString().isEmpty()) {
            UiHelper.showTextError(lastNameTV, UiHelper.errorForField("Last name", empty));
            return false;
        }

        if (phoneNumberTV.getText().toString().isEmpty()) {
            UiHelper.showTextError(phoneNumberTV, UiHelper.errorForField("Phone number", empty));
            return false;
        }

        if (dateOfBirthBtn.getText().toString().isEmpty()) {
            UiHelper.showTextError(dateOfBirthTV, UiHelper.errorForField("Date of birth", empty));
            return false;
        }
        return true;
    }

    private String getStringResource(int strRes) {
        return getResources().getString(strRes);
    }

}
