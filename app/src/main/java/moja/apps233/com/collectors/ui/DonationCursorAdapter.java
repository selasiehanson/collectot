package moja.apps233.com.collectors.ui;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import moja.apps233.com.collectors.R;

/**
 * Created by selasiehanson on 12/02/2016.
 */
public class DonationCursorAdapter extends CursorAdapter {
    public DonationCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.collection_row, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tvFullName = (TextView) view.findViewById(R.id.tvFullName);
        TextView tvGender = (TextView) view.findViewById(R.id.tvGender);
        TextView tvDateOfBirth = (TextView) view.findViewById(R.id.tvDateOfBirth);
        TextView tvPhoneNumber = (TextView) view.findViewById(R.id.tvPhoneNumber);

        // Extract properties from cursor
        String firstName = cursor.getString(cursor.getColumnIndexOrThrow("first_name"));
        String lastName = cursor.getString(cursor.getColumnIndexOrThrow("last_name"));
        String gender = cursor.getString(cursor.getColumnIndexOrThrow("gender"));
        String phoneNumber = cursor.getString(cursor.getColumnIndexOrThrow("phone_number"));
        String dateOfBirth = cursor.getString(cursor.getColumnIndexOrThrow("date_of_birth"));
        String bloodGroup = cursor.getString(cursor.getColumnIndexOrThrow("blood_group"));

        // Populate fields with extracted properties
        String fullName = String.format("%s %s", firstName, lastName);

        tvFullName.setText(fullName);
        tvGender.setText(gender);
        tvDateOfBirth.setText(dateOfBirth);
        tvPhoneNumber.setText(phoneNumber);

    }
}
