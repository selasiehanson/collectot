package moja.apps233.com.collectors.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import moja.apps233.com.collectors.R;
import moja.apps233.com.collectors.models.Donation;
import moja.apps233.com.collectors.net.ApiHandle;
import moja.apps233.com.collectors.net.DataService;
import moja.apps233.com.collectors.net.request_objects.DonationDTO;
import moja.apps233.com.collectors.net.response_objects.DonationResponse;
import moja.apps233.com.collectors.util.StorageUtil;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by selasiehanson on 09/02/2016.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    private static final int SECONDS_PER_MINUTE = 60;
    private Context context;
    private static final String LOG_TAG = "SYNC_ADAPTER";

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        this.context = context;
    }


    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.d(LOG_TAG, "----------SENDING DATA TO SERVER--------");
        pushListToServer();
        EventBus.getDefault().post(new MessageEvent("Hello everyone!"));
    }

    private void pushListToServer() {
        List<Donation> donations = new Select().from(Donation.class).execute();
        if (donations.size() == 0) return;
        List<DonationDTO> donationDTOs = new ArrayList<>();
        for (Donation donation : donations) {
            donationDTOs.add(donation.toDonationDTO());
        }

        Retrofit retrofit = ApiHandle.getRetrofit(StorageUtil.getToken(getContext()));
        DataService service = retrofit.create(DataService.class);
        service.uploadDonations(donationDTOs).enqueue(new Callback<DonationResponse>() {
            @Override
            public void onResponse(Response<DonationResponse> response, Retrofit retrofit) {
                //remove donations from device or update them to synced
                if (response.isSuccess()) {
                    ActiveAndroid.beginTransaction();
                    try {
                        for (Donation d : donations) {
                            String clause = String.format("%s = ?", BaseColumns._ID);
                            new Delete().from(Donation.class).where(clause, d.getId()).execute();
                        }
                        ActiveAndroid.setTransactionSuccessful();
                    } finally {
                        ActiveAndroid.endTransaction();
                        EventBus.getDefault().post(new MessageEvent("Hello everyone!"));
                    }
                }


            }

            @Override
            public void onFailure(Throwable t) {

            }
        });


    }

    public static void initializeSyncAdapter(Context context) {
        getSyncAccount(context);
    }

    public static Account getSyncAccount(Context context) {
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        // Create the account type and default account
        Account newAccount = new Account(
                context.getString(R.string.app_name), context.getString(R.string.sync_account_type));

        // If the password doesn't exist, the account doesn't exist
        if (null == accountManager.getPassword(newAccount)) {

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                return null;
            }
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */

            onAccountCreated(newAccount, context);
        }
        return newAccount;
    }


    public static void syncImmediately(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(getSyncAccount(context), context.getString(R.string.content_authority), bundle);
    }

    private static int getSyncInterval(int minutes) {
        return SECONDS_PER_MINUTE * minutes;
    }

    private static int getSyncFlextime(int syncInterval) {
        return syncInterval / 3;
    }

    private static void onAccountCreated(Account newAccount, Context context) {
        Log.d(LOG_TAG, "Syncing Called - onAccountCreated");
        final int SYNC_INTERVAL = getSyncInterval(1);
        final int SYNC_FLEXTIME = getSyncFlextime(SYNC_INTERVAL);

        SyncAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), true);
        syncImmediately(context);
    }


    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {
        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.content_authority);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            SyncRequest request = new SyncRequest.Builder()
                    .syncPeriodic(syncInterval, flexTime)
                    .setSyncAdapter(account, authority)
                    .setExtras(new Bundle()).build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account, authority, new Bundle(), syncInterval);
        }
    }

    // TODO: 09/02/2016  1. Fetch token after timeout. 2. push resources to server


}
