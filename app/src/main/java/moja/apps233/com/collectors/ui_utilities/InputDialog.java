package moja.apps233.com.collectors.ui_utilities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import moja.apps233.com.collectors.R;


public class InputDialog extends DialogFragment {
    private EditText inputValueTV;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("URL");

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.fragment_input_dialog, null);
        inputValueTV = (EditText) view.findViewById(R.id.inputValueTV);
//        inputValueTV.setText(APP_KEYS.BACK_END_URL);
        builder.setView(view);

        builder.setPositiveButton("Set", (dialog, id) -> {
        });

        builder.setNegativeButton("Cancel", (dialog, id) -> InputDialog.this.getDialog().cancel());

        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            String url = inputValueTV.getText().toString();
            Boolean wantToCloseDialog = true;
            if (url.isEmpty()) {
                inputValueTV.setError("URL cannot be empty");
                wantToCloseDialog = false;
            }
//            APP_KEYS.BACK_END_URL = url;
//            Helper.storeAppUrl(getActivity(), url);
            if (wantToCloseDialog) {
                dismiss();
            }
        });
        return dialog;
    }
}
