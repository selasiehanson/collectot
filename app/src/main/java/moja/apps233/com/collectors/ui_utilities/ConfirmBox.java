package moja.apps233.com.collectors.ui_utilities;

import android.app.AlertDialog;
import android.content.Context;

/**
 * Created by Albert on 14/12/2014.
 */
public class ConfirmBox {

    private AlertDialog confirm;
    private boolean isConfirmed;

    public ConfirmBox(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setMessage(message)
                .setNegativeButton("No", (dialog, which) -> isConfirmed = false)
                .setPositiveButton("Yes", (dialog, id) -> isConfirmed = true);
        confirm = builder.create();
    }

    public void show() {
        if (confirm != null){
            confirm.show();
        }
    }

    public boolean isConfirmed(){
        return isConfirmed;
    }
}
