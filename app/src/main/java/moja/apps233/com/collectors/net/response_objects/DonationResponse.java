package moja.apps233.com.collectors.net.response_objects;

/**
 * Created by selasiehanson on 12/02/2016.
 */
public class DonationResponse {
    public boolean success;
    public Integer status;
    public String error;
    public String message;
}
