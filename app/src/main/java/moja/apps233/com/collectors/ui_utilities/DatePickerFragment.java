package moja.apps233.com.collectors.ui_utilities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Albert on 27/11/2014.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    static Context sContext;
    static Calendar sDate;
    static DateDialogFragmentListener sListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int year = sDate.get(Calendar.YEAR);
        int month = sDate.get(Calendar.MONTH);
        int day = sDate.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(sContext, this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar newDate = Calendar.getInstance();
        newDate.set(year, monthOfYear, dayOfMonth);
        sListener.dateDialogFragmentDateSet(newDate);
    }

    public static DatePickerFragment newInstance(Context context, int titleResource, Calendar date) {
        DatePickerFragment dialog = new DatePickerFragment();
        sContext = context;
        sDate = date;
        Bundle data = new Bundle();
        data.putInt("title", titleResource);
        dialog.setArguments(data);
        return dialog;
    }


    public void setDateDialogFragmentListener(DateDialogFragmentListener listener) {
        sListener = listener;
    }

}
