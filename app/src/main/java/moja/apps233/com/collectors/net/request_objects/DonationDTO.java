package moja.apps233.com.collectors.net.request_objects;

import java.util.Date;

/**
 * Created by selasiehanson on 09/02/2016.
 */
public class DonationDTO {


    private Long donationCentreId;

    private String donationType;

    private Long localDonorId;

    private Long mobileDonorId;

    private Long appointmentId;

    private Date donationDate;

    private LocalDonorDTO localDonor;

    public DonationDTO() {

    }

    public Long getDonationCentreId() {
        return donationCentreId;
    }

    public void setDonationCentreId(Long donationCentreId) {
        this.donationCentreId = donationCentreId;
    }

    public String getDonationType() {
        return donationType;
    }

    public void setDonationType(String donationType) {
        this.donationType = donationType;
    }

    public Long getLocalDonorId() {
        return localDonorId;
    }

    public void setLocalDonorId(Long localDonorId) {
        this.localDonorId = localDonorId;
    }

    public Long getMobileDonorId() {
        return mobileDonorId;
    }

    public void setMobileDonorId(Long mobileDonorId) {
        this.mobileDonorId = mobileDonorId;
    }

    public Long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Date getDonationDate() {
        return donationDate;
    }

    public void setDonationDate(Date donationDate) {
        this.donationDate = donationDate;
    }

    public LocalDonorDTO getLocalDonor() {
        return localDonor;
    }

    public void setLocalDonor(LocalDonorDTO localDonor) {
        this.localDonor = localDonor;
    }
}
