package moja.apps233.com.collectors.models;

import android.database.Cursor;
import android.provider.BaseColumns;
import android.util.Log;

import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.Date;

import moja.apps233.com.collectors.net.request_objects.DonationDTO;
import moja.apps233.com.collectors.net.request_objects.LocalDonorDTO;
import moja.apps233.com.collectors.util.Config;
import moja.apps233.com.collectors.util.UiHelper;

/**
 * Created by selasiehanson on 09/02/2016.
 */

@Table(name = "donations", id = BaseColumns._ID)
public class Donation extends Model {

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "gender")
    private String gender;

    @Column(name = "date_of_birth")
    private String dateOfBirth;

    @Column(name = "blood_group")
    private String bloodGroup;

    @Column(name = "donation_centre_id")
    private Long donationCentreId;

    @Column(name = "donation_type")
    private String donationType;

    @Column(name = "local_donor_id")
    private Long localDonorId;

    @Column(name = "donation_date")
    private Long donationDate;

    @Column(name = "has_synced")
    private transient boolean synced;

    @Column(name = "remote_id")
    private Long remoteId;

    @Column(name = "mobile_created_at")
    private Long createdAt;

    @Column(name = "mobile_updated_at")
    private Long updatedAt;


    public Donation() {
        super();
        this.localDonorId = null;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public Long getDonationCentreId() {
        return donationCentreId;
    }

    public void setDonationCentreId(Long donationCentreId) {
        this.donationCentreId = donationCentreId;
    }

    public String getDonationType() {
        return donationType;
    }

    public void setDonationType(String donationType) {
        this.donationType = donationType;
    }

    public Long getLocalDonorId() {
        return localDonorId;
    }

    public void setLocalDonorId(Long localDonorId) {
        this.localDonorId = localDonorId;
    }

    public Long getDonationDate() {
        return donationDate;
    }

    public void setDonationDate(Long donationDate) {
        this.donationDate = donationDate;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public Long getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(Long remoteId) {
        this.remoteId = remoteId;
    }


    public static Cursor donationCursor() {
        String tableName = Cache.getTableInfo(Donation.class).getTableName();
        String sql = String.format("%s.*, %s._id as _id", tableName, tableName);
        String query = new Select(sql).from(Donation.class).toSql();
        Log.d("SQL", query);
        Cursor resultCursor = Cache.openDatabase().rawQuery(query, null);
        return resultCursor;

    }

    public void localSave() {
        this.synced = false;
        if (this.createdAt == null) {
            this.createdAt = System.currentTimeMillis();
        }
        this.updatedAt = this.createdAt = System.currentTimeMillis();
        this.save();
    }

    public DonationDTO toDonationDTO() {
        DonationDTO dto = new DonationDTO();
        dto.setDonationCentreId(50L);

        Date d = new Date(getDonationDate());
        dto.setDonationDate(d);
        dto.setDonationType("FROM_FIELD");

        LocalDonorDTO ldDTO = new LocalDonorDTO();
        ldDTO.setFirstName(getFirstName());
        ldDTO.setLastName(getLastName());
        ldDTO.setGender(getGender());

        Date db = UiHelper.fromFormDateToJavaDate(getDateOfBirth()).getTime();
        ldDTO.setDateOfBirth(UiHelper.formatDate(db, Config.DATE_FORMATS.YEAR_MONTH_DAY));

        ldDTO.setPhoneNumber(getPhoneNumber());
        ldDTO.setBloodGroup(getBloodGroup());

        dto.setLocalDonor(ldDTO);
        return dto;
    }

}
