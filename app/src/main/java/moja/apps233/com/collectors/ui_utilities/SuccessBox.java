package moja.apps233.com.collectors.ui_utilities;

import android.app.AlertDialog;
import android.content.Context;

import moja.apps233.com.collectors.R;


/**
 * Created by selasiehanson on 7/31/15.
 */
public class SuccessBox {
    private AlertDialog alert;

    public SuccessBox(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setIcon(R.mipmap.ic_done_black);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", (dialog, id) -> dialog.dismiss());
        alert = builder.create();
    }

    public void show() {
        if (alert != null) {
            alert.show();
        }
    }
}
