package moja.apps233.com.collectors.sync;

public class SyncEvent {

    public SyncEvent(String type, String message) {
        this.message = message;
        this.type = type;
    }

    private String type;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
